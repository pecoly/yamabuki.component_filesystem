﻿using System.Windows.Forms;

using NUnit.Framework;

using Yamabuki.Component.FileSystem;
using Yamabuki.Test.View;

namespace Yamabuki.Component.FileSystemTest
{
    [TestFixture]
    public class StGetDirectories_VTest
    {
        private StGetDirectories_V view = new StGetDirectories_V();

        [Test]
        public void StartPosition()
        {
            Assert.AreEqual(view.StartPosition, FormStartPosition.CenterScreen);
        }

        [Test]
        public void BorderStyle()
        {
            Assert.AreEqual(view.FormBorderStyle, FormBorderStyle.FixedSingle);
        }

        [Test]
        public void MaximizeBox()
        {
            Assert.AreEqual(view.MaximizeBox, false);
        }

        [Test]
        public void Control()
        {
            ViewTest.Control(view.Controls);
        }

        [Test]
        public void KryptonButton()
        {
            ViewTest.KryptonButton(view.Controls);
        }
    }
}
