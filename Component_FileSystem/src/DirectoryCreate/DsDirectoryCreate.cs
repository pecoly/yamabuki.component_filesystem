﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.SimpleEditForm;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.Func;
using Yamabuki.Utility.Log;

namespace Yamabuki.Component.FileSystem
{
    public class DsDirectoryCreate
        : DsSimpleComponent_1_0
    {
        /// <summary>ロガー</summary>
        private static readonly Logger Logger = LogManager.GetLogger(
            MethodBase.GetCurrentMethod().DeclaringType);

        public override String TypeName
        {
            get { return "フォルダ作成"; }
        }

        public override int DefaultWidth
        {
            get { return 108; }
        }

        internal String Description
        {
            get
            {
                return "フォルダを作成します。\r\n" +
                    "フォルダパスは絶対パスで指定してください。\r\n" +
                    "例 : \r\n" +
                    "C:\\abc";
            }
        }

        public override BaseMessage DoubleClick()
        {
            using (var presenter = new DsSimpleEditForm_P(this))
            {
                presenter.Show(FormSize.Auto, this.TypeName, this.Description);
            }

            return null;
        }

        protected override void Initialize_1_0()
        {
        }

        protected override TsTask GetTask(TsDataStoreGuid inputDataStoreGuidList)
        {
            var action = new Action<List<String>>(x =>
            {
                x.ForEach(y =>
                {
                    if (!Path.IsPathRooted(y))
                    {
                        throw new ArgumentException(
                            "フォルダパスは絶対パスを指定してください。フォルダパス : " + y);
                    }

                    if (!Directory.Exists(y))
                    {
                        Directory.CreateDirectory(y);
                    }
                });
            });
            
            return new TsFuncTask_1_0__N__<String>(
                this.DefinitionPath,
                inputDataStoreGuidList,
                action);
        }
    }
}
