﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.SimpleEditForm;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.Func;
using Yamabuki.Utility.Log;

namespace Yamabuki.Component.FileSystem
{
    public class DsGetFileSize
        : DsSimpleComponent_1_1
    {
        /// <summary>ロガー</summary>
        private static readonly Logger Logger = LogManager.GetLogger(
            MethodBase.GetCurrentMethod().DeclaringType);

        public override String TypeName
        {
            get { return "ファイルサイズ取得"; }
        }

        public override int DefaultWidth
        {
            get { return 144; }
        }

        internal String Description
        {
            get
            {
                return "ファイルサイズを取得します。\r\n" + 
                    "2ギガバイト以上のファイルサイズは取得出来ません。\r\n" + 
                    "ファイルパスは絶対パスで指定してください。\r\n" +
                    "例 : \r\n" + 
                    "C:\\abc\\def.txt";
            }
        }
        
        public override BaseMessage DoubleClick()
        {
            using (var presenter = new DsSimpleEditForm_P(this))
            {
                presenter.Show(FormSize.Auto, this.TypeName, this.Description);
            }

            return null;
        }
        
        protected override void Initialize_1_1()
        {
        }

        protected override TsTask GetTask(
            TsDataStoreGuid inputDataStoreGuidList,
            TsDataStoreGuid outputDataStoreGuidList)
        {
            var func = new Func<List<String>, List<Int32>>(x =>
            {
                x.ForEach(y => 
                {
                    if (!Path.IsPathRooted(y))
                    {
                        throw new ArgumentException(
                            "ファイルパスは絶対パスを指定してください。ファイルパス : " + y);
                    }

                    if (!File.Exists(y))
                    {
                        throw new ArgumentException(
                            "ファイルが存在しません。ファイルパス : " + y);
                    }
                });

                return x.Select(y => Convert.ToInt32(new FileInfo(y).Length)).ToList();
            });

            return new TsFuncTask_1_1__N_N<String, Int32>(
                this.DefinitionPath,
                inputDataStoreGuidList,
                outputDataStoreGuidList,
                func);
        }
    }
}
