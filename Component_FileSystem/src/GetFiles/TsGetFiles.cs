﻿using System;
using System.Collections.Generic;
using System.IO;

using Yamabuki.Task.Data;
using Yamabuki.Task.ExceptionEx;
using Yamabuki.Task.Value;

namespace Yamabuki.Component.FileSystem
{
    internal class TsGetFiles
        : TsValueTask_1_1__1_N<String, String>
    {
        private Boolean includingSubDirectory;

        public TsGetFiles(
            String definitionPath,
            TsDataStoreGuid inputDataStoreGuid,
            TsDataStoreGuid outputDataStoreGuid,
            Boolean includingSubDirectory)
            : base(definitionPath, inputDataStoreGuid, outputDataStoreGuid)
        {
            this.includingSubDirectory = includingSubDirectory;
        }

        internal List<String> GetFiles(String directoryPath)
        {
            if (!Directory.Exists(directoryPath))
            {
                throw new Exception("フォルダが存在しません。フォルダパス : " + directoryPath);
            }

            if (this.Executer.Cancel)
            {
                throw new TsCancelException(this.DefinitionPath);
            }

            var result = new List<String>();
            var fileList = Directory.GetFiles(directoryPath);
            var directoryList = Directory.GetDirectories(directoryPath);
            result.AddRange(fileList);

            if (this.includingSubDirectory)
            {
                foreach (var x in directoryList)
                {
                    result.AddRange(this.GetFiles(x));
                }
            }

            return result;
        }

        protected override List<String> ExecuteInternal(String inputValue)
        {
            return this.GetFiles(inputValue);
        }
    }
}
