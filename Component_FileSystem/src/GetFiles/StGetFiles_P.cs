﻿using System;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.FileSystem
{
    internal class StGetFiles_P
        : DsEditForm_P<StGetFiles_VI, DsGetFiles>
    {
        public StGetFiles_P(DsGetFiles com)
            : base(com)
        {
        }

        public override void Load()
        {
            this.View.Initialize();
            this.View.IncludingSubDirectory = this.Component.IncludingSubDirectory;
        }

        public override void Save()
        {
            this.Component.IncludingSubDirectory = this.View.IncludingSubDirectory;
        }
    }
}
