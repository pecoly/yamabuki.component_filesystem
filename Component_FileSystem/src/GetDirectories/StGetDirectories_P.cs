﻿using System;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.FileSystem
{
    internal class StGetDirectories_P
        : DsEditForm_P<StGetDirectories_VI, DsGetDirectories>
    {
        public StGetDirectories_P(DsGetDirectories com)
            : base(com)
        {
        }

        public override void Load()
        {
            this.View.Initialize();
            this.View.IncludingSubDirectory = this.Component.IncludingSubDirectory;
        }

        public override void Save()
        {
            this.Component.IncludingSubDirectory = this.View.IncludingSubDirectory;
        }
    }
}
