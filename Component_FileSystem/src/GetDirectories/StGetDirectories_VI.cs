﻿using System;
using System.Text;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.FileSystem
{
    internal interface StGetDirectories_VI
        : DsEditForm_VI
    {
        Boolean IncludingSubDirectory { get; set; }
    }
}
