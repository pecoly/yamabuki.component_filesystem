﻿using System;
using System.Collections.Generic;
using System.IO;

using Yamabuki.Task.Data;
using Yamabuki.Task.ExceptionEx;
using Yamabuki.Task.Value;

namespace Yamabuki.Component.FileSystem
{
    internal class TsGetDirectories
        : TsValueTask_1_1__1_N<String, String>
    {
        private Boolean includingSubDirectory;

        public TsGetDirectories(
            String definitionPath,
            TsDataStoreGuid inputDataStoreGuid,
            TsDataStoreGuid outputDataStoreGuid,
            Boolean includingSubDirectory)
            : base(definitionPath, inputDataStoreGuid, outputDataStoreGuid)
        {
            this.includingSubDirectory = includingSubDirectory;
        }

        internal List<String> GetDirectories(String directoryPath)
        {
            if (!Directory.Exists(directoryPath))
            {
                throw new Exception("フォルダが存在しません。フォルダパス : " + directoryPath);
            }

            if (this.Executer.Cancel)
            {
                throw new TsCancelException(this.DefinitionPath);
            }

            var result = new List<String>();
            var directoryList = Directory.GetDirectories(directoryPath);
            result.AddRange(directoryList);

            if (this.includingSubDirectory)
            {
                foreach (var x in directoryList)
                {
                    result.AddRange(this.GetDirectories(x));
                }
            }

            return result;
        }

        protected override List<String> ExecuteInternal(String inputValue)
        {
            return this.GetDirectories(inputValue);
        }
    }
}
