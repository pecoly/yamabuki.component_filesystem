﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Xml.Linq;

using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.Message;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Utility.Cast;
using Yamabuki.Utility.Log;
using Yamabuki.Window.Core;

namespace Yamabuki.Component.FileSystem
{
    public class DsGetDirectories
        : DsSimpleComponent_1_1
    {
        /// <summary>ロガー</summary>
        private static readonly Logger Logger = LogManager.GetLogger(
            MethodBase.GetCurrentMethod().DeclaringType);

        public override String TypeName
        {
            get { return "フォルダ一覧取得"; }
        }

        public override int DefaultWidth
        {
            get { return 132; }
        }

        internal Boolean IncludingSubDirectory { get; set; }

        public override void Initialize(IEnumerable<XElement> data)
        {
            foreach (var e in data)
            {
                if (e.Name == Property.IncludingSubDirectory)
                {
                    this.IncludingSubDirectory = CastUtils.ToBoolean(e.Value, (x) => false);
                }
            }
        }

        public override BaseMessage DoubleClick()
        {
            var oldHasHeader = this.IncludingSubDirectory;
            var oldE = this.GetXElement();
            var result = this.ShowDialog();

            var isUpdated = result == FormResult.Ok;
            if (!isUpdated)
            {
                return null;
            }

            return new UpdateComponentMessage(oldE, this.GetXElement());
        }

        public override void SetParameter(IDictionary<String, String> paramList)
        {
            foreach (var kv in paramList)
            {
                if (kv.Key == Property.IncludingSubDirectory)
                {
                    this.IncludingSubDirectory = CastUtils.ToBoolean(kv.Value, (x) => false);
                }
            }
        }

        public override IEnumerable<XElement> DataToXElement()
        {
            var list = new List<XElement>();
            list.Add(new XElement(Property.IncludingSubDirectory, this.IncludingSubDirectory));
            return list;
        }
        
        protected override void Initialize_1_1()
        {
        }

        protected virtual FormResult ShowDialog()
        {
            using (var presenter = new StGetDirectories_P(this))
            using (var view = new StGetDirectories_V())
            {
                presenter.View = view;
                view.ShowDialog();
                return view.Result;
            }
        }

        protected override TsTask GetTask(
            TsDataStoreGuid inputDataStoreGuidList,
            TsDataStoreGuid outputDataStoreGuidList)
        {
            return new TsGetDirectories(
                this.DefinitionPath,
                inputDataStoreGuidList,
                outputDataStoreGuidList,
                this.IncludingSubDirectory);
        }

        /// <summary>プロパティ</summary>
        private struct Property
        {
            internal const String IncludingSubDirectory = "IncludingSubDirectory";
        }
    }
}
