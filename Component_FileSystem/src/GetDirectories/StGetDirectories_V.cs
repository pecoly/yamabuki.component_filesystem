﻿using System;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.FileSystem
{
    internal partial class StGetDirectories_V
        : DsEditForm_V, StGetDirectories_VI
    {
        private EventHandler okayButton_Click;

        private EventHandler cancelButton_Click;

        public StGetDirectories_V()
        {
            this.InitializeComponent();
        }

        public Action OkButton_Click
        {
            set
            {
                this.okButton.Click -= this.okayButton_Click;
                this.okayButton_Click = (sender, e) => value();
                this.okButton.Click += this.okayButton_Click;
            }
        }

        public Action CancelButton_Click
        {
            set
            {
                this.cancelButton.Click -= this.cancelButton_Click;
                this.cancelButton_Click = (sender, e) => value();
                this.cancelButton.Click += this.cancelButton_Click;
            }
        }

        public String Message
        {
            set { this.statusLabel.Text = value; }
        }

        public Boolean IncludingSubDirectory
        {
            get { return this.includingSubDirectoryCheckButton.Checked; }
            set { this.includingSubDirectoryCheckButton.Checked = value; }
        }

        public override void Initialize()
        {
            this.includingSubDirectoryCheckButton.Focus();

            this.UpdateIncludingSubDirectoryCheckButtonText();

            this.includingSubDirectoryCheckButton.CheckedChanged +=
                (sender, e) => this.UpdateIncludingSubDirectoryCheckButtonText();
        }

        private void UpdateIncludingSubDirectoryCheckButtonText()
        {
            if (this.includingSubDirectoryCheckButton.Checked)
            {
                this.includingSubDirectoryCheckButton.Text = "含む";
            }
            else
            {
                this.includingSubDirectoryCheckButton.Text = "含まない";
            }
        }
    }
}
