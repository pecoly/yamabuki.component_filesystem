﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.SimpleEditForm;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.Func;
using Yamabuki.Utility.Log;

namespace Yamabuki.Component.FileSystem
{
    public class DsFileExists
        : DsSimpleComponent_1_1
    {
        /// <summary>ロガー</summary>
        private static readonly Logger Logger = LogManager.GetLogger(
            MethodBase.GetCurrentMethod().DeclaringType);

        public override String TypeName
        {
            get { return "ファイル有無"; }
        }

        public override int DefaultWidth
        {
            get { return 108; }
        }

        internal String Description
        {
            get
            {
                return "ファイルが存在するかどうかを確認します。\r\n" + 
                    "ファイルパスは絶対パスで指定してください。\r\n" +
                    "例 : \r\n" + 
                    "C:\\abc\\def.txt";
            }
        }
        
        public override BaseMessage DoubleClick()
        {
            using (var presenter = new DsSimpleEditForm_P(this))
            {
                presenter.Show(FormSize.Auto, this.TypeName, this.Description);
            }

            return null;
        }
        
        protected override void Initialize_1_1()
        {
        }

        protected override TsTask GetTask(
            TsDataStoreGuid inputDataStoreGuidList,
            TsDataStoreGuid outputDataStoreGuidList)
        {
            var func = new Func<List<String>, List<Boolean>>(x =>
            {
                x.ForEach(y => 
                {
                    if (!Path.IsPathRooted(y))
                    {
                        throw new ArgumentException(
                            "ファイルパスは絶対パスを指定してください。ファイルパス : " + y);
                    }
                });

                return x.Select(y => File.Exists(y)).ToList();
            });
            
            return new TsFuncTask_1_1__N_N<String, Boolean>(
                this.DefinitionPath,
                inputDataStoreGuidList,
                outputDataStoreGuidList,
                func);
        }
    }
}
