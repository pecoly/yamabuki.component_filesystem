﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.SimpleEditForm;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.ExceptionEx;
using Yamabuki.Task.Func;
using Yamabuki.Utility.Log;

namespace Yamabuki.Component.FileSystem
{
    public class DsPathCombine
        : DsSimpleComponent_2_1
    {
        /// <summary>ロガー</summary>
        private static readonly Logger Logger = LogManager.GetLogger(
            MethodBase.GetCurrentMethod().DeclaringType);

        public override String TypeName
        {
            get { return "パス連結"; }
        }

        public override int DefaultWidth
        {
            get { return 96; }
        }

        internal String Description
        {
            get
            {
                return "2つのパスを1つのパスに結合します。\r\n" +
                    "例 : \r\n" +
                    "入力1  : C:\\abc\r\n" +
                    "入力2  : def.txt\r\n" +
                    "出力 : C:\\abc\\def.txt";
            }
        }

        public override BaseMessage DoubleClick()
        {
            using (var presenter = new DsSimpleEditForm_P(this))
            {
                presenter.Show(FormSize.Auto, this.TypeName, this.Description);
            }

            return null;
        }
        
        protected override void Initialize_2_1()
        {
        }

        protected override TsTask GetTask(
            TsDataStoreGuid inputDataStoreGuidList0,
            TsDataStoreGuid inputDataStoreGuidList1,
            TsDataStoreGuid outputDataStoreGuidList)
        {
            var func = new Func<List<String>, List<String>, List<String>>((p0, p1) =>
            {
                if (p0.Count != p1.Count)
                {
                    throw new TsInputDataInvalidLengthException(
                        this.DefinitionPath,
                        "パス1のデータ長とパス2のデータ長が異なります。");
                }
                
                return p0.Zip(p1, Tuple.Create).Select(x => Path.Combine(x.Item1, x.Item2)).ToList();
            });
            
            return new TsFuncTask_2_1__NN_N<String, String, String>(
                this.DefinitionPath,
                inputDataStoreGuidList0,
                inputDataStoreGuidList1,
                outputDataStoreGuidList,
                func);
        }
    }
}
