﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.SimpleEditForm;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.Func;
using Yamabuki.Utility.Log;

namespace Yamabuki.Component.FileSystem
{
    public class DsGetFileNameWithoutExtension
        : DsSimpleComponent_1_1
    {
        /// <summary>ロガー</summary>
        private static readonly Logger Logger = LogManager.GetLogger(
            MethodBase.GetCurrentMethod().DeclaringType);

        public override String TypeName
        {
            get { return "ファイル名取得(拡張子なし)"; }
        }

        public override int DefaultWidth
        {
            get { return 192; }
        }

        internal String Description
        {
            get
            {
                return "ファイルパスから拡張子を除いた\r\n" + 
                    "ファイル名を取得します。\r\n" +
                    "例 : \r\n" +
                    "入力 : C:\\abc\\def.txt\r\n" +
                    "出力 : def";
            }
        }

        public override BaseMessage DoubleClick()
        {
            using (var presenter = new DsSimpleEditForm_P(this))
            {
                presenter.Show(FormSize.Auto, this.TypeName, this.Description);
            }

            return null;
        }
        
        protected override void Initialize_1_1()
        {
        }

        protected override TsTask GetTask(
            TsDataStoreGuid inputDataStoreGuidList,
            TsDataStoreGuid outputDataStoreGuidList)
        {
            var func = new Func<List<String>, List<String>>(x =>
            {
                return x.Select(y => Path.GetFileNameWithoutExtension(y == null ? "" : y)).ToList();
            });
            
            return new TsFuncTask_1_1__N_N<String, String>(
                this.DefinitionPath,
                inputDataStoreGuidList,
                outputDataStoreGuidList,
                func);
        }
    }
}
